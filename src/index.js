import React from 'react';
import ReactDOM from 'react-dom';

import './css/bootstrap.min.css';
import './css/index.css';

import Field from './components/field'

ReactDOM.render(
    <Field />,
    document.getElementById('root')
);
