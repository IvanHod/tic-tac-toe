import React from 'react';
import Message from './message';
import Socket from './socket';
import ComeMessage from '../../sounds/comeMessage.mp3';
import SendMessage from '../../sounds/sendMessage.mp3';

/**
 * Класс для отображения сообщений
 * */
export default class Body extends React.Component {
    constructor() {
        super();
        this.state = {
            messages: []
        };
        this.comeMessage = new  Audio(ComeMessage);
        this.sendMessage = new Audio(SendMessage);
        this.socket = Socket.getInstance();
        let parent = this;
        this.socket.on('new-message', (to, message) => {
            let messages = parent.state.messages;
            if(to === parent.socket.id)
                parent.sendMessage.play();
            else
                parent.comeMessage.play();
            messages.push({
                isSelf: to === parent.socket.id,
                message: message
            });
            parent.setState({
                messages: messages
            });
        });
    }

    /**
     * Скролл панели до нового сообщения
     * */
    scrollElement() {
        window.requestAnimationFrame(function() {
            let node = document.getElementById('chat-body');
            if (node !== undefined) {
                node.scrollTop = node.scrollHeight;
            }
        });
    }

    componentDidUpdate() {
        this.scrollElement();
        if(this.props.systemMessage) {
            let messages = this.state.messages;
            messages.push({
                isSystem: true,
                message: this.props.systemMessage
            });
            this.setState({
                messages: messages
            });
        }
    }

    render() {
        let messages = this.state.messages;
        return (
            <div className="panel-body" id="chat-body">
                {messages.map((message, i) => <Message
                    key={i.toString()}
                    isSystem={message.isSystem}
                    isSelf={message.isSelf}
                    message={message.message}
                />)}
            </div>
        );
    }
}