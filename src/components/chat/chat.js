import React from 'react';
import Footer from './footer';
import Body from './body';
import Socket from './socket';

/**
 * Основной класс для отображения чата
 * */
export default class Chat extends React.Component {
    constructor() {
        super();
        this.state = {
            isEnemy: false,
            systemMessage: ''
        };
        let socket = Socket.getInstance();
        let parent = this;
        socket.on('connect', () => {
            socket.emit('join-to-room', socket.room);
        });
        socket.on('enemy-set', () => {
            parent.setState({isEnemy: true, systemMessage: 'Подсоединился новый соперник'});
        });
        socket.on('enemy-disconnect', () => {
            parent.setState({isEnemy: false, systemMessage: 'Ваш соперник отсоединился'});
        });
    }

    componentDidUpdate() {
        if(this.state.systemMessage !== '')
            this.setState({systemMessage: ''});
    }

    render() {
        return (
            <div className="panel">
                <div className="panel-default">
                    <div className="panel-heading">
                        <div className="panel-title">Чат с соперником
                            <span>
                                <span className="small text-danger pull-right">{this.state.isEnemy ? '' : 'нет противника'}</span>
                            </span>
                        </div>
                    </div>
                    <Body
                        systemMessage={this.state.systemMessage}
                    />
                    <Footer />
                </div>
            </div>
        )
    }
}
