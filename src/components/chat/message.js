import React from 'react';

/**
 * Класс для отображения одного сообщения
 * */
export default class Message extends React.Component {
    constructor() {
        super();
        this.MAX_SYMBOLS_STR = 30;
    }

    /**
     * Добавить в сообщение переводы строк
     *
     * @param {String} message сообщение
     * @return {String} сообщение с переводами строк
     * */
    splitMessage(message) {
        let paths = message.replace('  ', ' ').split(' ');
        let result = [''];
        let parent = this;
        paths.forEach((path) => {
            let lastIndex = result.length;
            if((result[lastIndex - 1] + path).length > parent.MAX_SYMBOLS_STR) {
                result.push(path);
            }
            else {
                result[lastIndex - 1] += ' ' + path;
            }
        });
        return result.join('</br>');
    }

    render() {
        let props = this.props;
        let divClass = 'message text-';
        let labelClass = 'label label-';
        if(props.isSystem) {
            divClass += 'center';
            labelClass = 'label system-message';
        }
        else {
            divClass += props.isSelf ? 'right' : 'left';
            labelClass += props.isSelf ? 'info' : 'success';
        }
        return (
            <div className={divClass}>
                <div className={labelClass} dangerouslySetInnerHTML={{__html: this.splitMessage(this.props.message)}} />
            </div>
        );
    }
}