import React from 'react';
import Socket from './socket';


/**
 * Класс для отображения блока с вводом информации
 * */
export default class Footer extends React.Component {
    constructor() {
        super();
        this.ENTER_CODE = 13;
        this.socket = Socket.getInstance();
        this.state = {
            input: ''
        }
    }

    /**
     * Нажатие клавишей на input
     * */
    inputPress(e) {
        if(e.charCode === this.ENTER_CODE) {
            this.sendMessage(e.target.value);
        }
    }

    /**
     * Отправить сообщение
     * */
    sendMessage(input) {
        if(input instanceof Object)
            input = input.currentTarget.previousSibling.value;
        if(input) {
            this.socket.emit('send-message', this.socket.room, input);
            document.getElementById('input-message').value = '';
        }
    }

    render() {
        return (
            <div className="panel-footer">
                <div className="input-group">
                    <input type="text" className="form-control" id="input-message" onKeyPress={this.inputPress.bind(this)}/>
                    <span className="input-group-addon send-btn" onClick={this.sendMessage.bind(this)}>
                        <span className="glyphicon glyphicon-envelope"></span>
                    </span>
                </div>
            </div>
        )
    }
}