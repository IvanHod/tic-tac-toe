import OpenSocket from 'socket.io-client';
let Config = require('../../config');

/**
 * Класс для создание соккета для игры
 * */
export default class Socket {
    static instance = null;

    static getInstance() {
        if (this.instance === null) {
            let params = window.location.search.match(/room=[a-zA-Z0-9]+/);
            const room = params && params[0] ? params[0].replace('room=', '') : '';

            this.instance = OpenSocket('http://localhost:' + Config.socket.port + '/game', {
                query: {
                    'room': room
                }
            });

            this.instance.room = room;
        }

        return this.instance;
    }
}