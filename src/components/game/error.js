import React from 'react';
import Socket from './socket.js';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

/**
 * Класс для отображение ошибки
 * */
export default class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {items: []};

        let socket = Socket.getInstance();
        let parent = this;
        socket.on('error-one-player', (error) => {
            parent.setState({
                items: [error]
            });
        });
    }

    handleRemove() {
        this.setState({items: []});
    }

    render() {
        let parent = this;
        if(this.state.items.length) {
            setTimeout(() => {
                parent.handleRemove();
            }, 2000);
        }
        const items = this.state.items.map((item, i) => (
            <div className="alert alert-danger text-center" key={item}>{item}</div>
        ));
        return (
            <div>
                <ReactCSSTransitionGroup
                    transitionName="error"
                    transitionEnter={false}
                    transitionLeaveTimeout={300}>
                    {items}
                </ReactCSSTransitionGroup>
            </div>
        );
    }
}