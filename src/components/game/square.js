import React from 'react';
import SVGX from '../../svg/labelX.svg';
import SVGO from '../../svg/labelO.svg';
import ReactSVG from 'react-svg';

/**
 * Класс игровой клетки
 * */
export default class Square extends React.Component {
    constructor() {
        super();
        this.label = {
            'X': SVGX,
            'O': SVGO,
            '': ''
        }
    }

    renderSVG(label) {
        if (this.label[label]) {
            return <ReactSVG
                path={this.label[label]}
                callback={this.svgIsRender.bind(this)}
                className={'pos-' + this.props.pos}
            />
        }
    }

    svgIsRender(svg) {
        let className = svg.getAttribute("class");
        let square = svg.parentNode.parentNode.parentNode.className;
        if(square.indexOf('notVisible') >= 0 && className.indexOf('pos-' + this.props.pos) >= 0) {
            svg.style.visibility = 'inherit';
            if(className.indexOf('cross') >= 0) {
                this.showElement(svg.querySelector('.secondPath'));
                let secondLine = svg.querySelector('.firstPath');
                secondLine.style.stroke = '#fff';

                let parent = this;
                setTimeout(() => {
                    secondLine.style.stroke = '#000';
                    parent.showElement(secondLine);
                }, 500);
            } else {
                this.showElement(svg.querySelector('.circle'));
            }
        }
    }

    showElement(element) {
        if(element) {
            let length = 500;

            element.style.strokeDasharray = length + ' ' + length;
            element.style.strokeDashoffset = length;

            element.getBoundingClientRect();

            element.style.transition = element.style.WebkitTransition = 'stroke-dashoffset 1s';

            element.style.strokeDashoffset = '0';
        }
    }

    render() {
        let className = 'square' + (this.props.isVisible ? '' : ' notVisible');
        return (
            <div className={className} onClick={() => this.props.onClick()}>
                {this.renderSVG(this.props.value)}
            </div>
        );
    }
}