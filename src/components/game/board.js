import React from 'react';
import Square from './square';
import Socket from './socket.js';
import soundFile from '../../sounds/playerMoved.mp3';

/**
 * Класс игровой площадки
 * */
export default class Board extends React.Component {
    constructor() {
        super();
        this.state = {
            squares: new Array(9).fill(null),
            isNext: null
        };

        this.socket = Socket.getInstance();
        this.sound = new Audio(soundFile);

        let parent = this;
        this.socket.on('made-move', (i, label) => {
            const squares = parent.state.squares.slice();
            squares[i] = label;
            parent.lastPos = i;
            parent.setState({
                squares: squares,
                isNext: parent.props.label !== label,
            });
            if(parent.label !== label)
                parent.sound.play();
        });
        this.socket.on('new-game', () => {
            parent.setState({
                squares: new Array(9).fill(null),
                isNext: parent.props.label === 'X'
            });
        });
        this.socket.on('clear-board', () => {
            parent.setState({
                squares: new Array(9).fill(null),
                isNext: true
            });
        });
    }

    handleClick(i) {
        const squares = this.state.squares.slice();
        if (squares[i]) {
            return;
        }
        this.socket.emit('made-move', i);
    }

    renderSquare(i) {
        return <Square
            pos={i}
            value={this.state.squares[i]}
            isVisible={this.lastPos !== i}
            onClick={() => this.handleClick(i)}
        />;
    }

    render() {
        let status = this.state.isNext ? 'Ваш ход' : 'Ход противника';

        if(this.state.isNext === null)
            status = (this.props.label === 'X' ? 'Ваш ход' :'Ход противника');
        return (
            <div>
                <div className="h4 form-group text-center">{status}</div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}