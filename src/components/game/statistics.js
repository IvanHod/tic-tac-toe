import React from 'react';
import Socket from './socket.js';

export default class Field extends React.Component {
    constructor() {
        super();
        let socket = Socket.getInstance();
        this.state = {
            currentLabel: '',
            score: {
                'X': 0,
                'O': 0
            }
        };

        let parent = this;
        socket.on('set-label', (label) => {
            this.setState({
                currentLabel: label
            });
        });
        socket.on('winner', (winner) => {
            let score = parent.state.score;
            parent.setState({
                score: {
                    'X': score['X'] + (winner === 'X' ? 1 : 0),
                    'O': score['O'] + (winner === 'O' ? 1 : 0)
                }
            });
        });
        socket.on('enemy-disconnect', () => {
            parent.setState({
                score: {
                    'X': 0,
                    'O': 0
                }
            });
        });
    }
    render() {
        let winX = this.state.score['X'] ? this.state.score['X'] : '-';
        let winO = this.state.score['O'] ? this.state.score['O'] : '-';
        let isCurrentLabelX = this.state.currentLabel === 'X' ? ' active-label' : '';
        let isCurrentLabelO = this.state.currentLabel === 'O' ? 'active-label' : '';
        return (
            <div className="panel">
                <div className="panel-default">
                    <div className="panel-heading">
                        <div className="panel-title">Статистика игры</div>
                    </div>
                    <div className="panel-body">
                        <div className="form-group">
                            <span className={isCurrentLabelX}>Игрок X</span>
                            <span className="pull-right">{winX}</span>
                        </div>
                        <div>
                            <span className={isCurrentLabelO}>Игрок O</span>
                            <span className="pull-right">{winO}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}