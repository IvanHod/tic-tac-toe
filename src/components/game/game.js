import React from 'react';
import Board from './board';
import Error from './error';
import Socket from './socket.js';
import ReactSVG from 'react-svg';
import WinnerLineV from '../../svg/winnerLineV.svg';
import WinnerLineH from '../../svg/winnerLineH.svg';
import WinnerLineToBL from '../../svg/winnerLineToBL.svg';
import WinnerLineToBR from '../../svg/winnerLineToBR.svg';

/**
 * Основной игровой класс
 * */
export default class Game extends React.Component {
    constructor() {
        super();
        this.state = {
            isFinish: false,
            label: null
        };

        /**
         * number - разница между x2 и x1
         * */
        this.winnerLine = {
            1: WinnerLineH,
            2: WinnerLineToBL,
            3: WinnerLineV,
            4: WinnerLineToBR
        };

        this.socket = Socket.getInstance();

        let parent = this;
        this.socket.on('set-label', (label) => {
            parent.setState({label: label});
        });
        this.socket.on('winner', (winner, x1, x2, x3) => {
            parent.winner = winner;
            parent.coords = {x1: x1, x2: x2, x3: x3};
            parent.setIsFinish(true);
        });
        this.socket.on('new-game', () => {
            parent.setIsFinish(false);
        });
        this.socket.on('enemy-disconnect', () => {
            parent.setIsFinish(false);
        });
    }

    setIsFinish(isFinish) {
        this.setState({isFinish: isFinish});
    }

    newGame() {
        this.socket.emit('ready');
        document.getElementById('is-ready').innerText = '(Ожидание второго игрока...)';
    }

    renderWinnerLine() {
        if(this.winner) {
            let style = {};
            let diff = this.coords['x2'] - this.coords['x1'];
            if(diff === 1) {
                style = {paddingTop: ((this.coords['x1']/3) * 120) + 'px'};
            } else if(diff === 3) {
                style = {paddingLeft: ((this.coords['x1']) * 120) + 'px'};
            }
            return <ReactSVG
                path={this.winnerLine[this.coords['x2'] - this.coords['x1']]}
                className="winnerLine"
                style={style}
            />
        }
    }

    renderResult() {
        if(this.state.isFinish) {
            let result = 'Ничья!';
            if(this.winner)
                result = this.state.label === this.winner ? 'Вы выиграли =)' : 'Вы проиграли =(';

            let resultClass = 'h1 result-' + (!this.winner || this.state.label === this.winner ? 'win' : 'fail');
            return (
                <div className="finish-window" onClick={this.newGame.bind(this)}>
                    <div className="finish-window-background" />
                    <div className="finish-window-text">
                        <div className="finish-text text-center">
                            <div className={resultClass}>{result}</div>
                            <div className="h2">Новая игра?</div>
                            <div id="is-ready">&nbsp;</div>
                        </div>
                    </div>
                    {this.renderWinnerLine()}
                </div>
            );
        }
    }

    render() {
        return (
            <div>
                <Error />
                {this.renderResult()}
                <div className="game-board">
                    <Board
                        label={this.state.label}
                    />
                </div>
            </div>
        );
    }
}
