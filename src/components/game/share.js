import React from 'react';

/**
 * Класс отвечающий за элемент "Поделиться"
 * */
export default class Share extends React.Component {
    copyUrl(e) {
        let href = e.target.previousSibling;
        let range = document.createRange();
        range.selectNode(href);
        window.getSelection().addRange(range);
        try {
            document.execCommand('copy');
        } catch (err) {
            console.error(err);
        }
        window.getSelection().removeAllRanges();
    }

    render() {
        return (
            <div className="alert alert-warning">
                <div className="row">
                    <div className="col-md-12">
                        Ссылка для подключения другого игрока:
                        <a href={this.props.value}> {this.props.value}</a>
                        <span className="btn btn-default pull-right" onClick={this.copyUrl}>Копировать</span>
                    </div>
                </div>
            </div>
        );
    }
}