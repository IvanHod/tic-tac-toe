import React from 'react';
import Game from './game/game';
import Chat from './chat/chat';
import Share from './game/share';
import Statistic from './game/statistics';
import GameSocket from './game/socket';
import ChatSocket from './chat/socket';

/**
 * Класс для отображение всего поля и включающих компонентов
 * */
export default class Field extends React.Component {
    constructor() {
        super();
        let gameSocket = GameSocket.getInstance();
        let chatSocket = ChatSocket.getInstance();
        this.state = {
            room: gameSocket.room,
            isEnemy: false,
            score: {
                'X': 0,
                'Y': 0
            }
        };
        let parent = this;
        gameSocket.on('set-room', (room) => {
            chatSocket.room = room;
            parent.setState({room: room});
            chatSocket.emit('join-to-room', room);
            window.history.pushState(null, null, '?room=' + room);
        });
        gameSocket.on('enemy-set', () => {
            parent.setState({isEnemy: true});
        });
        gameSocket.on('enemy-disconnect', () => {
            parent.setState({isEnemy: false});
        });
        gameSocket.on('room-is-fill', () => {
            window.location.href = window.location.origin;
        });
    }

    renderShare() {
        if(!this.state.isEnemy && this.state.room) {
            let host = 'http://localhost:3000/?room=' + this.state.room;
            return <Share
                value={host}
            />
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row form-group">
                    <div className="col-md-4">
                        <Statistic />
                        <Chat />
                    </div>
                    <div className="col-md-4">
                        <Game
                            isDisconnect={this.state.isEnemy}
                        />
                    </div>
                    <div className="col-md-4 text-center h4">Видео чат</div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        {this.renderShare()}
                    </div>
                </div>
            </div>
        )
    }
}