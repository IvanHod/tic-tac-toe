let config = require('./config');
const io = require('socket.io')();
let Rooms = require('../models/rooms');

let rooms = new Rooms();

let nsGame = io.of('/game');

nsGame.on('connection', (socket) => {
    let room = rooms.addRoom(socket.request._query.room, (room) => {
        socket.emit('set-room', room);
    });

    let player = rooms.addPlayer(room, socket.id, (err, label, enemySet) => {
        if(err) {
            room = null;
            socket.emit('room-is-fill');
            return;
        }
        socket.join(room);
        socket.emit('set-label', label);
        if(enemySet) {
            nsGame.to(room).emit('enemy-set');
        }
    });

    socket.on('made-move', function(pos) {
        let game = rooms.getGame(room);
        if(!game.canPlay()) {
            socket.emit('error-one-player', 'Для игры необходимо два участника');
            return;
        }
        if(player && player.isNext) {
            let res = game.setLabel(pos, player.getLabel());

            if(res) {
                game.changeQueues();
                nsGame.to(room).emit('made-move', pos, player.getLabel());
            }

            let winner = game.determineWinner((winner, i1, i2, i3) => {
                nsGame.to(room).emit('winner', winner, i1, i2, i3);
            });
            if(winner) {
                game.clear();
            }
        }
    });
    socket.on('ready', function () {
        let game = rooms.getGame(room);
        game.playerReady(socket.id, () => {
            nsGame.to(room).emit('new-game');
        });
    });

    socket.on('disconnect', function () {
        if(room) {
            let game = rooms.getGame(room);
            game.removePlayer(socket.id);
            game.makeFirstPlayerGeneral((label) => {
                // изменить, только одному игроку устанавливать label
                nsGame.to(room).emit('set-label', label);
            });
            game.clear();
            nsGame.to(room).emit('clear-board');
            nsGame.to(room).emit('enemy-disconnect');
        }
    });
});

let nsChat = io.of('/chat');

nsChat.on('connection', (socket) => {
    let room = null;
    socket.on('join-to-room', (newRoom) => {
        room = newRoom;
        socket.join(newRoom);

        let game = rooms.existGame(room);
        if(game && game.getCountPlayers() === game.MAX_PLAYERS) {
            nsChat.to(room).emit('enemy-set');
        }
    });
    socket.on('send-message', function(room, message) {
        nsChat.to(room).emit('new-message', socket.id, message);
    });
    socket.on('disconnect', () => {
        nsChat.to(room).emit('enemy-disconnect');
    });
});

io.listen(config.socket.port);

console.log('connection to port: ' + config.socket.port);