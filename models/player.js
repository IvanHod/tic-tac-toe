/*
* Класс для игрока
*
* */
class Player {
    /**
     * @param {int} id индентификатор игрока
     * @param {String} label метка игрока
     * @param {Boolean} isNext Признак, что следующий ход принадлежит этому игроку
     * */
    constructor(id, label, isNext) {
        this.id = id;
        this.label = label;
        this.isNext = isNext;
        this.isReady = false;
    }

    /**
     * @param {String} label метка игрока
     * */
    setLabel(label) {
        this.label = label;
    }

    /**
     * @return {String} получить метку игрока
     * */
    getLabel() {
        return this.label;
    }

    /**
     * Изменить признак очереди хода
     * @param {Boolean} queue признак следующего хода
     * */
    changeQueue(queue) {
        if(queue === undefined || queue === null)
            queue = !this.isNext;
        this.isNext = queue;
    }
}

module.exports = Player;