let Player = require('./player');

/**
 * Основной игровой класс
 * */
class Game {
    constructor() {
        this.SIZE_FIELD = 9;
        this.squares = new Array(this.SIZE_FIELD).fill(null);
        this.MAX_PLAYERS = 2;
        this.players = {};
        this.labels = ['X', 'O'];
    }

    /**
     * Добавить нового игрока в игру
     *
     * @param {int} id socket.id игрока
     * @param {function} callback
     * @return {Object|null} созданный игрок
     * */
    addPlayer(id, callback) {
        if(this.getCountPlayers() >= this.MAX_PLAYERS) {
            let error = {msg: 'Достигнут лимит игроков в комнате'};
            callback(error);
            return null;
        }
        let player = new Player(id, this.labels[this.getCountPlayers()], this.getCountPlayers() === 0);
        this.players[id] = player;
        callback(null, player.getLabel(), this.getCountPlayers() === 2);
        return player;
    }

    /**
     * Устанавливить метку на указанную позицию поля
     *
     * @param {int} pos позиция игрового поля
     * @param {String} label устанавливаемая метка
     * @return {Boolean} Признак, что метка установлена
     * */
    setLabel(pos, label) {
        if(this.squares[pos] !== null) {
            return false;
        }
        this.squares[pos] = label;
        return true;
    }

    /**
     * Получить текущее количество игроков
     *
     * @return {int} количество игроков
     * */
    getCountPlayers() {
        return Object.keys(this.players).length;
    }

    /**
     * Признак, можно ли ввести игру
     *
     * @return {Boolean} можно ли ввести игру
     * */
    canPlay() {
        return this.getCountPlayers() === this.MAX_PLAYERS;
    }

    /**
     * Получить игрока по индексу
     * @param {int} number индекс игрока
     * @return {Object|null} игрок
     * */
    getPlayer(number) {
        let key = Object.keys(this.players)[number];
        if(key && this.players[key])
            return this.players[key];
    }

    /**
     * Удалить игрока по id
     * @param {int} id индентификатор игрока
     * */
    removePlayer(id) {
        let player = this.players[id];
        delete this.players[id];
    }

    /**
     * Отменить готовность игрока к новой игре
     *
     * @param {int} id индентификатор игрока
     * @parem {function} callback
     * */
    playerReady(id, callback) {
        this.players[id].isReady = true;
        let isReady = true;
        for(let key in this.players) {
            isReady &= this.players[key].isReady;
        }
        if(isReady) {
            for(let key in this.players) {
                this.players[key].isReady = false;
            }
            this.clear();
            callback();
        }
    }

    /**
     * Сделать первого игрока главным
     * @param {function} callback
     * */
    makeFirstPlayerGeneral(callback) {
        let player = this.getPlayer(0);
        if(player) {
            player.setLabel(this.labels[0]);
            player.changeQueue(true);
        }
        callback(this.labels[0]);
    }

    /**
     * Изменить очередь игроков
     * */
    changeQueues() {
        let players = this.players;
        for(let key in players) {
            players[key].changeQueue();
        }
    }

    /**
     * Очистить игровое поле
     * */
    clear() {
        this.squares = new Array(9).fill(null);
        let players = this.players;
        for(let key in players) {
            players[key].isNext = players[key].getLabel() === 'X';
        }
    }

    /**
     * Определить победителя
     *
     * @param {function} callback
     * @return {String|null} Метка выигревшего игрока
     * */
    determineWinner(callback) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (this.squares[a] && this.squares[a] === this.squares[b] && this.squares[a] === this.squares[c]) {
                callback(this.squares[a], a, b, c);
                return this.squares[a];
            }
        }

        let countLabels = 0;
        for(let i = 0; i < this.SIZE_FIELD; i++)
            if(this.squares[i])
                countLabels++;
        if(countLabels === this.SIZE_FIELD) {
            callback(false);
            return '-';
        }
        return null;
    }
}

module.exports = Game;