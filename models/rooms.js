let Game = require('./game');
let randomstring = require('randomstring');

/**
 * @class Rooms класс хранящий комнаты и позволяющий манипулировать ими
 * */
class Rooms {
    constructor() {
        this.rooms = {};
    }

    /**
     * Добавить комнату
     *
     * @param {null|int} room идентификатор комнаты
     * @param {function} callback
     * @return {int} идентификатор комнаты
     * */
    addRoom(room, callback) {
        if(!room) {
            room = randomstring.generate();
            callback(room);
        }
        if(!this.rooms[room])
            this.rooms[room] = {game: null};
        return room;
    }

    /**
     * Добавить игру в комнату
     *
     * @param {int} room идентификатор комнаты
     * @return {Object} экземпляр класса Game
     * */
    addGame(room) {
        let game = this.rooms[room].game;
        if(!game)
            game = new Game(room);
        this.rooms[room] = {game: game};
        return game;
    }

    /**
     * Проверить, существует ли комната, и вернуть её если она существует
     *
     * @param {int} room идентификатор комнаты
     * @return {Object} экземпляр класса Game
     * */
    existGame(room) {
        let game = null;
        if(this.rooms[room] && this.rooms[room].game)
            game = this.rooms[room].game;
        return game;
    }

    /**
     * Получить игру
     *
     * @param {int} room идентификатор комнаты
     * @return {Object} экземпляр класса Game
     * */
    getGame(room) {
        return this.rooms[room].game;
    }

    /**
     * Добавить игрока в игру
     *
     * @param {int} room идентификатор комнаты
     * @param {int} id идентификатор игрока
     * @param {function} callback
     * @return {Object} экземпляр класса PLayer
     * */
    addPlayer(room, id, callback) {
        let game = this.rooms[room].game;
        if(!game)
            game = this.addGame(room);
        let player = game.addPlayer(id, callback);
        return player;
    }
}

module.exports = Rooms;